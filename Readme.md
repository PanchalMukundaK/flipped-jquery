#Flip jQuery


###What is Flip?
Flip is a jQuery plugin that will flip easily your elements in four directions. Try it.

###How to use?
Like every jquery plugin, just chain it:
<pre><code>$("#flipbox").flip({
  direction:'tb'
})
</code></pre>

###How to change content?
Add content params in this way:

  <pre><code>$("#flipbox").flip({
    direction:'tb',
    content:'this is my new content'
  })</code></pre>

###How to add callbacks?
There are three available callbacks: onBefore, onAnimation, onEnd

<pre><code>$("#flipbox").flip({
	direction:'tb',
	onBefore: function(){
			console.log('before starting the animation');
	},
	onAnimation: function(){
			console.log('in the middle of the animation');
	},
	onEnd: function(){
			console.log('when the animation has already ended');
	}
})</code></pre>

###How to revert a flip?

There's an "hidden" method called revertFlip: as it says, revert a flip to the previous state

<pre><code>$("#flipbox").revertFlip()</code></pre>

##All options

###Here are all options available:

#####content-
define the new content of the flipped box. It works with: html, text or a jQuery object ex:$("selector")

#####direction-
the direction where to flip. Possible values: 'tb', 'bt', 'lr', 'rl' (default:'tb')

#####color-
Flip element ending background color

#####speed-
Speed of the two parts of the animation

#####onBefore-
Synchronous function excuted before the animation starts

#####onAnimation-
Synchronous function excuted at half animation

#####onEnd-
Synchronous function excuted after animation's end

###Compatibility:
####Flip is compatible with:
Firefox, Chrome/Chromium, Opera, Safari and even IE (6,7,8)
####License:
MIT/GPL license

###Notes:
The font used in this page is Chunk, made by Meredith Mandel available at The league of movable type.
Webfont-kit provided by Font Squirrel
This page is using Remy Sharp's HTML5 shiv to enable HTML5 elements on IE
